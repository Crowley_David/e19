package com.inf5d6.tp1.repositories

import android.app.Application
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.inf5d6.tp1.MainActivity
import com.inf5d6.tp1.MainActivity.Companion.SRVURL
import org.json.JSONObject

class LoginRepository(val app: Application){
    fun login(username: String, password:String) {
        val body = JSONObject()
        body.put("username", username)
        body.put("password", password)

        val queue = Volley.newRequestQueue(app)
        val url = SRVURL + "/auth/token"
        val postRequest = JsonObjectRequest(
            Request.Method.POST,
            url,
            body,
            {
                // it.getString(“token”) contient le jeton
                Log.d("Jeton :", it.getString("token"))
                MainActivity.TOKEN = it.getString("token")
                var intent = Intent(app, MainActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                app.startActivity(intent)
            },
            {
                Log.d("Error.Response", it.message.toString())
                Toast.makeText(app, "L'usager ou le mot de passe est inexact : " + it.message, Toast.LENGTH_SHORT).show()
            }
        )
        queue.add(postRequest)
    }
}