package com.inf5d6.tp1.repositories

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.inf5d6.tp1.MainActivity
import com.inf5d6.tp1.models.DetailsTvShow
import com.inf5d6.tp1.models.Role
import com.inf5d6.tp1.models.Season
import com.inf5d6.tp1.models.TvShow
import com.inf5d6.tp1.ui.details.DetailsFragment
import com.squareup.picasso.Picasso

class DetailsRepository (private val application: Application) {

    fun getDetails(tvShow: MutableLiveData<DetailsTvShow>, tvshowId : String) {

        val queue = Volley.newRequestQueue(application)
        val r = StringRequest(
            Request.Method.GET,
            MainActivity.SRVURL + "/tvshow?tvshowId=" + tvshowId,
            {
                val gson = Gson()
                val details  = gson.fromJson(it, DetailsTvShow::class.java)
                tvShow.value = details
            },
            {
                Toast.makeText(application, "Erreur de connection : " + it.message, Toast.LENGTH_SHORT).show()
            })
        queue.add(r)
    }
}