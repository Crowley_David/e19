package com.inf5d6.tp1.repositories

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.inf5d6.tp1.MainActivity.Companion.SRVURL
import com.inf5d6.tp1.models.TvShow

class TvShowRepository (private val application: Application) {
    fun getTvShows(tvShows: MutableLiveData<MutableList<TvShow>>) {

        val queue = Volley.newRequestQueue(application)
        val r = StringRequest(
            Request.Method.GET,
            SRVURL + "/tvshows",
            {
                val arrayTvShows= Gson().fromJson(it, Array<TvShow>::class.java)
                tvShows.postValue(arrayTvShows.toMutableList())
            },
            {
                Toast.makeText(application, "Erreur de connection : " + it.message, Toast.LENGTH_SHORT).show()
            })
        queue.add(r)
    }
}