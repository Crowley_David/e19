package com.inf5d6.tp1.repositories

import android.app.Application
import android.util.Log
import android.util.MutableBoolean
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.android.volley.Request
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.inf5d6.tp1.MainActivity
import com.inf5d6.tp1.MainActivity.Companion.TOKEN
import com.inf5d6.tp1.models.DetailsTvShow
import com.inf5d6.tp1.models.Favorite
import com.inf5d6.tp1.models.TvShow

class FavoriteRepository (private val application: Application) {

    fun getFavorite(favorites: MutableLiveData<MutableList<TvShow>>) {

        val queue = Volley.newRequestQueue(application)
        val r = object : StringRequest(
            Request.Method.GET,
            MainActivity.SRVURL + "/favorites",
            {
                val arrayTvShows = Gson().fromJson(it, Array<TvShow>::class.java)
                favorites.postValue(arrayTvShows.toMutableList())
            },
            {
                Toast.makeText(
                    application,
                    "Erreur de connection : " + it.message,
                    Toast.LENGTH_SHORT
                ).show()
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headerMap = mutableMapOf<String, String>()
                headerMap.put("Content-Type", "application/json");
                headerMap.put("Authorization", "Bearer ${TOKEN}");
                return headerMap
            }
        }
        queue.add(r)
    }
    fun getFavs(tvShowList: MutableLiveData<MutableList<TvShow>>) {
        val queue = Volley.newRequestQueue(application)
        val url = "${MainActivity.SRVURL}/favorites"
        val request = object : JsonArrayRequest(
            Method.GET,
            url,
            null,
            { response->
                val favorites =
                    Gson().fromJson(response.toString(), Array<TvShow>::class.java)
                tvShowList.value = favorites.toMutableList()
                Log.println(Log.INFO, "REFRESH", response.toString())
            },
            { error->
                println(error)

            },
        ) {
            override fun getHeaders(): MutableMap<String, String> {
                val headerMap = mutableMapOf<String, String>()
                headerMap.put("Content-Type", "application/json")
                headerMap.put("Authorization", "Bearer $TOKEN")
                return headerMap
            }
        }
        queue.add(request)
    }
    fun favoritCheck(isFavorite : MutableLiveData<Favorite>, id : String){
        val queue = Volley.newRequestQueue(application);
        val url = "${MainActivity.SRVURL}/favorite?tvshowId=$id"
        val stringRequest = object: StringRequest(
            Method.GET,
            url,
            {
                val gson = Gson()
                val details = gson.fromJson(it, Favorite::class.java)
                isFavorite.value = details
            },
            {
                Toast.makeText(
                    application,
                    "Erreur de connection : " + it.message,
                    Toast.LENGTH_SHORT
                ).show()
            },
        ){
            override fun getHeaders(): MutableMap<String, String> {
                val headerMap = mutableMapOf<String, String>()
                headerMap.put("Content-Type", "application/json");
                headerMap.put("Authorization", "Bearer $TOKEN");
                return headerMap
            }
        }
        queue.add(stringRequest)
    }
    fun addFavoris(tvshowId: String) {
        val queue = Volley.newRequestQueue(application)
        val r = object : StringRequest(
            Request.Method.POST,
            MainActivity.SRVURL + "/favorite?tvshowId=" + tvshowId,
            {
                Toast.makeText(application, "Favorite added", Toast.LENGTH_SHORT).show()
            },
            {
                Toast.makeText(
                    application,
                    "Erreur de connection : " + it.message,
                    Toast.LENGTH_SHORT
                ).show()
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headerMap = mutableMapOf<String, String>()
                headerMap.put("Content-Type", "application/json");
                headerMap.put("Authorization", "Bearer ${MainActivity.TOKEN}");
                return headerMap
            }
        }
        queue.add(r)
    }

    fun deleteFavoris(tvshowId: String) {
        val queue = Volley.newRequestQueue(application)
        val r = object : StringRequest(
            Request.Method.DELETE,
            MainActivity.SRVURL + "/favorite?tvshowId=" + tvshowId,
            {
                Toast.makeText(application, "Favorite removed", Toast.LENGTH_SHORT).show()
            },
            {
                Toast.makeText(
                    application,
                    "Erreur de connection : " + it.message,
                    Toast.LENGTH_SHORT
                ).show()
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headerMap = mutableMapOf<String, String>()
                headerMap.put("Content-Type", "application/json");
                headerMap.put("Authorization", "Bearer ${MainActivity.TOKEN}");
                return headerMap
            }
        }
        queue.add(r)
    }
}