package com.inf5d6.tp1.models

data class Favorite (val isFavorite: Boolean)