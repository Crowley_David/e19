package com.inf5d6.tp1

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel
    lateinit var etUser : EditText
    private lateinit var etPass : EditText
    lateinit var btnlogin : Button

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        this.loginViewModel =
            ViewModelProvider(this).get(LoginViewModel::class.java)

        btnlogin = findViewById(R.id.btn_submit)
        etPass = findViewById(R.id.et_password)
        etUser = findViewById(R.id.et_user_name)

        btnlogin.setOnClickListener{
            loginViewModel.login(etUser.text.toString(), etPass.text.toString())
        }

    }

}