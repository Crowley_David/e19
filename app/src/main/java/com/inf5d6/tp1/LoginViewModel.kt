package com.inf5d6.tp1

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.inf5d6.tp1.repositories.LoginRepository

class LoginViewModel(val app: Application) : AndroidViewModel(app) {

    val repoLogin : LoginRepository = LoginRepository(app)

    fun login(username: String, password:String) {
        repoLogin.login(username, password)
    }


}

