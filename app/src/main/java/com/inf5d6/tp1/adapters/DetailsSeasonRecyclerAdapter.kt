package com.inf5d6.tp1.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.inf5d6.tp1.R
import com.inf5d6.tp1.models.Season
import com.squareup.picasso.Picasso

class DetailsSeasonRecyclerAdapter (private val season: MutableList<Season>) :

    RecyclerView.Adapter<DetailsSeasonRecyclerAdapter.DetailSeasonViewHolder>() {

    class DetailSeasonViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailSeasonViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycler_seasons, parent, false)
        return DetailSeasonViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: DetailSeasonViewHolder, position: Int) {

        val imgSeasonShow = holder.view.findViewById<ImageView>(R.id.rv_iv_seasons)
        Picasso.get().load(this.season[position].imgURL).into(imgSeasonShow)

        holder.view.findViewById<TextView>(R.id.rv_episode_number).text =
            this.season[position].episodeCount.toString() + " Episodes"
        holder.view.findViewById<TextView>(R.id.tv_season_number).text =
            "Season " +  this.season[position].number.toString()

    }

    override fun getItemCount(): Int {
        return this.season.size
    }
}