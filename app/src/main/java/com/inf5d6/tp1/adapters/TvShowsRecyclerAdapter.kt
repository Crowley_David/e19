package com.inf5d6.tp1.adapters

import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.inf5d6.tp1.R
import com.inf5d6.tp1.models.TvShow
import com.inf5d6.tp1.ui.details.DetailsFragment
import com.squareup.picasso.Picasso

class TvShowsRecyclerAdapter (private val tvshowsList: MutableList<TvShow>) :
    RecyclerView.Adapter<TvShowsRecyclerAdapter.TvShowViewHolder>() {

    class TvShowViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TvShowViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.tvshows_recyclerlayout, parent, false)
        return TvShowViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: TvShowViewHolder, position: Int) {
        val imgTvShow = holder.view.findViewById<ImageView>(R.id.iv_tvshow_item)
        Picasso.get().load(this.tvshowsList[position].imgURL).into(imgTvShow)
        val id = this.tvshowsList[position].tvshowId

        imgTvShow.setOnClickListener{
            val param = bundleOf(Pair("tvshowId", id))
            it.findNavController().navigate(R.id.navigation_details, param)
        }

}

    override fun getItemCount(): Int {
        return this.tvshowsList.size
    }
}