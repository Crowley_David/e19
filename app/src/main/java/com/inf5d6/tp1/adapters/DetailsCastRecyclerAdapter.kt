package com.inf5d6.tp1.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.inf5d6.tp1.R
import com.inf5d6.tp1.models.DetailsTvShow
import com.inf5d6.tp1.models.Role
import com.squareup.picasso.Picasso

class DetailsCastRecyclerAdapter (private val roles: MutableList<Role>) :

    RecyclerView.Adapter<DetailsCastRecyclerAdapter.DetailCastViewHolder>() {

    class DetailCastViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailCastViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycler_cast, parent, false)
        return DetailCastViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: DetailCastViewHolder, position: Int) {

        val imgCastShow = holder.view.findViewById<ImageView>(R.id.rv_iv_cast)
        Picasso.get().load(this.roles[position].imgURL).into(imgCastShow)

        holder.view.findViewById<TextView>(R.id.rv_tv_actor_name).text = this.roles[position].name
        holder.view.findViewById<TextView>(R.id.rv_tv_character).text = this.roles[position].character

    }

    override fun getItemCount(): Int {
        return this.roles.size
    }
}