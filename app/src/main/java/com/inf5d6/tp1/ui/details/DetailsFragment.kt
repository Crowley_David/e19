package com.inf5d6.tp1.ui.details

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.inf5d6.tp1.R
import com.inf5d6.tp1.adapters.DetailsCastRecyclerAdapter
import com.inf5d6.tp1.adapters.DetailsSeasonRecyclerAdapter
import com.inf5d6.tp1.models.Role
import com.inf5d6.tp1.models.Season
import com.inf5d6.tp1.repositories.FavoriteRepository
import com.squareup.picasso.Picasso



class DetailsFragment : Fragment() {

    private lateinit var rv_cast_show : RecyclerView
    private lateinit var rv_season_show : RecyclerView
    lateinit var favoriteRepository : FavoriteRepository

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //instancier le view
        val tvshowId = this.requireArguments().getInt("tvshowId").toString()
        val detailsFactory =
            DetailsFactory( application = requireActivity().application, tvshowId)
        val detailsViewModel =
            ViewModelProvider(this, detailsFactory).get(DetailsViewModel::class.java)

        rv_cast_show = view.findViewById(R.id.rv_cast_items)
        rv_season_show = view.findViewById(R.id.rv_seasons)
        rv_cast_show.layoutManager = LinearLayoutManager(context,  LinearLayoutManager.HORIZONTAL,false)
        rv_season_show.layoutManager = LinearLayoutManager(context,  LinearLayoutManager.HORIZONTAL,false)


        detailsViewModel.details.observe(viewLifecycleOwner) {

            Picasso.get().load(it.imgURL).into(view.findViewById<ImageView>(R.id.iv_details_img))
            view.findViewById<TextView>(R.id.tv_title).text = it.title
            view.findViewById<TextView>(R.id.tv_episodes).text = it.episodeCount.toString() + " Episodes"
            view.findViewById<TextView>(R.id.tv_description).text = it.plot

            rv_cast_show.adapter = DetailsCastRecyclerAdapter(it.roles as MutableList<Role>)
            rv_season_show.adapter = DetailsSeasonRecyclerAdapter(it.seasons as MutableList<Season>)
        }

        val btn_favorite = view.findViewById<ImageButton>(R.id.ib_favorite)

        detailsViewModel.isFavorite.observe(viewLifecycleOwner){

            if (it.isFavorite){
                btn_favorite.setTag("on")
                btn_favorite.setImageResource(android.R.drawable.btn_star_big_on)
            } else {
                btn_favorite.setTag("off")
                btn_favorite.setImageResource(android.R.drawable.btn_star_big_off)
            }

            btn_favorite.setOnClickListener(){
                if (btn_favorite.getTag() == "on"){
                    btn_favorite.setTag("off")
                    btn_favorite.setImageResource(android.R.drawable.btn_star_big_off)
                    detailsViewModel.removeFavorite(tvshowId)
                }else {
                    btn_favorite.setTag("on")
                    btn_favorite.setImageResource(android.R.drawable.btn_star_big_on)
                    detailsViewModel.addFavorite(tvshowId)
                }
            }
        }


    }

}