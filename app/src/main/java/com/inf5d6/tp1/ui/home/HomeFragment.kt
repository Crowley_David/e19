package com.inf5d6.tp1.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.inf5d6.tp1.R
import com.inf5d6.tp1.adapters.TvShowsRecyclerAdapter

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var iv_tvshow : RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        iv_tvshow = view.findViewById(R.id.frag_tvshow)
        iv_tvshow.layoutManager = GridLayoutManager(context, 2,LinearLayoutManager.VERTICAL,false)

        homeViewModel.tvShows.observe(viewLifecycleOwner) {
            this.iv_tvshow.adapter = TvShowsRecyclerAdapter(it)

        }

    }
}