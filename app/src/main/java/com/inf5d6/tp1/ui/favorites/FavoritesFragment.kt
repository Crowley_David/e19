package com.inf5d6.tp1.ui.favorites

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.inf5d6.tp1.R
import com.inf5d6.tp1.adapters.TvShowsRecyclerAdapter

class FavoritesFragment : Fragment() {

    private lateinit var favoritesViewModel: FavoritesViewModel
    lateinit var iv_favorite : RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_favorites, container, false)
    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        iv_favorite = view.findViewById(R.id.rv_favorite)
        iv_favorite.layoutManager = GridLayoutManager(context, 2,
            LinearLayoutManager.VERTICAL,false)

        this.favoritesViewModel =
            ViewModelProvider(this).get(FavoritesViewModel::class.java)
        favoritesViewModel.getFavs()
        favoritesViewModel.tvShowsFavorite.observe(viewLifecycleOwner) {
            this.iv_favorite.adapter = TvShowsRecyclerAdapter(it)

        }
    }

}