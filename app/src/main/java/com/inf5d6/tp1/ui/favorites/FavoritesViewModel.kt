package com.inf5d6.tp1.ui.favorites

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.inf5d6.tp1.models.DetailsTvShow
import com.inf5d6.tp1.models.TvShow
import com.inf5d6.tp1.repositories.FavoriteRepository
import com.inf5d6.tp1.repositories.TvShowRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FavoritesViewModel(val app: Application) : AndroidViewModel(app) {

    val tvShowsFavorite : MutableLiveData<MutableList<TvShow>> = MutableLiveData(mutableListOf())
    val favRepo = FavoriteRepository(getApplication())
    fun getFavs() {
        viewModelScope.launch(Dispatchers.IO) {
            favRepo.getFavs(tvShowsFavorite)
        }
    }
    init {
        viewModelScope.launch(Dispatchers.IO) {
            val favoritesRepository = FavoriteRepository(getApplication())
            favoritesRepository.getFavs(tvShowsFavorite)
        }
    }
}
