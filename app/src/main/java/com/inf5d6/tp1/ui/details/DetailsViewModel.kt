package com.inf5d6.tp1.ui.details

import android.app.Application
import android.util.MutableBoolean
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.inf5d6.tp1.models.DetailsTvShow
import com.inf5d6.tp1.models.Favorite
import com.inf5d6.tp1.models.Role
import com.inf5d6.tp1.models.TvShow
import com.inf5d6.tp1.repositories.DetailsRepository
import com.inf5d6.tp1.repositories.FavoriteRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailsViewModel  (val app: Application, id : String) : AndroidViewModel(app) {

    var details : MutableLiveData<DetailsTvShow> = MutableLiveData()
    var isFavorite : MutableLiveData<Favorite> = MutableLiveData()
    val favoriteRepository = FavoriteRepository(getApplication())

    init {
        viewModelScope.launch(Dispatchers.IO) {
            val detailsRepository = DetailsRepository(getApplication())
            val favoriteRepository = FavoriteRepository(getApplication())
            detailsRepository.getDetails(details, id)
            favoriteRepository.favoritCheck(isFavorite, id)
        }
    }
    fun addFavorite(id : String){
        viewModelScope.launch(Dispatchers.IO){
            favoriteRepository.addFavoris(id)
        }
    }
    fun removeFavorite(id : String){
        viewModelScope.launch(Dispatchers.IO){
            favoriteRepository.deleteFavoris(id)
        }
    }
}